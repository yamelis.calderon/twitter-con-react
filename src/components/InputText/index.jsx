import React  from 'react'
import './inputText.css'

function InputText({onSendText , userNameToReply , onCloseText }) 
{

		return(

				<form className="form" method='post' onSubmit={onSendText}>
					<textarea className="text" name="text">
						{(userNameToReply) ? `${userNameToReply} ` : ''}
					 </textarea>
					<div className="buttons">
						<button className="close" onClick={onCloseText}> Cerrar </button>
						<button className="send" type="submit"> Enviar </button>
					</div>
				</form>

			)

}

export default InputText