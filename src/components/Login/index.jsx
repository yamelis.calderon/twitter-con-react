import React from 'react'
import './login.css'

function Login({onAuth}){
	return(
				<div className="root6">
					<p className="text5"> 
						Inicia sesion con tu cuenta GitHub , para que puedas leer y escribir mensajes
					</p>
					<button
						className="boton5"
						onClick={onAuth}
					>
					<span className='fa fa-github '></span> Login con GitHub
					</button>
				</div>
			)
}

export default Login;