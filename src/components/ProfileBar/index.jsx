import React from 'react';
import { Link } from 'react-router-dom'
import './profilebar.css';

function ProfileBar({picture,username,onOpenText,onLogout}){
	return (
			<div className="root3">
				<Link to='/profile'>
					<figure>
						<img className="avatar1" src={picture} />
					</figure>
				</Link>
					<span className="username1"> Hola @{username}</span>
					<button onClick={onOpenText} className="btn">
					<span className="fa fa-lg fa-edit"> </span> Twee!
					</button>

					<button onClick={onLogout} className="btn">
					<span className="fa fa-sign-out"> </span> Salir
					</button>
			</div>
		);
}

export default ProfileBar;
