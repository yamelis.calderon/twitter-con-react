import React , {Component} from 'react'
import './profile.css'

class Profile extends Component {

	constructor(props){
		super(props)
	}

render() {
	return (

		<div className="root5">
			<img className="avatar3" src={this.props.picture ? this.props.picture : ""} />
			<span className="name"> {this.props.displayName ? this.props.displayName : ""}</span>
			<ul className="data">
				<li>
					<span className='fa fa-user'> </span>  { this.props.username ?  this.props.username : "" }
				</li>
				<li>
					<span className='fa fa-envelope'></span> {this.props.emailAddress ? this.props.emailAddress : ""}
				</li>
				<li>
					<span className='fa fa-map-marker'></span> {this.props.ubication ? this.props.ubication :""}
				</li>
			</ul>
		</div>
		)
}



}


export default  Profile