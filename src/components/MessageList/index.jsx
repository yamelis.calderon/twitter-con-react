import React  from 'react'
import Message from '../Message'
import './message-list.css'

function MessageList({messages,user,onRetweet,onFavorite,onReplyTweet}){

	return (
			<div className="root2">
			{messages.map(msg => {
				return (
				<Message 
				key ={msg.id}
				text = {msg.text}
				picture={msg.picture}
				displayName = {msg.displayName}
				username = {msg.username}
				date = {msg.date}
				user={user}
				numRetweets= {msg.retweets}
				numFavorites= {msg.favorites}
				onRetweet = {() => onRetweet(msg.id)}
				onFavorite = {() => onFavorite(msg.id)}
				onReplyTweet = {() => onReplyTweet(msg.id, msg.username)}
				/>
				)
			}).reverse()}
			</div>

			)
}

export default MessageList