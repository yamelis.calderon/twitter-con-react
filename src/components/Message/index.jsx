import React,{ Component} from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import  './message.css'
class Message extends Component {
	constructor(props){
	super(props)

	this.state = {
		pressFavorite: false ,
		pressReteweet : false
	}
	this.onPressRetweet = this.onPressRetweet.bind(this)
	this.onPressFavorite = this.onPressFavorite.bind(this)

	}

	onPressFavorite(){
		this.props.onFavorite()
		this.setState ({pressFavorite: true })  
	
	}

	onPressRetweet(){
		this.props.onRetweet()
		this.setState ({pressReteweet : true })  
	}
	render(){
	    let dateFormat = moment(this.props.date).fromNow()
	    let userLink = `/user/${this.props.username}`
	 
	return (
		<div className="root1">
			<div className="user">
				<Link to={{ pathname: userLink}} >
					<figure>
						<img className="avatar" src = {this.props.picture}></img>
					</figure>
				</Link>	
				<span className="displayname"> {this.props.displayName}</span>
				<span className="username"> {this.props.username}</span>
				<span className="date"> {dateFormat}</span>
			</div>
			<h3>{this.props.text}</h3>
			<div className="boton">
				<div
				 className="icon"
				 onClick={this.props.onReplyTweet}
				 >
				 <span className="fa fa-reply"> </span> 
				</div>
				<div 
				className={(this.state.pressReteweet) ? "verde" : ''}
				onClick ={this.onPressRetweet}>
				<span className="fa fa-retweet"></span> 
				<span className="num">{this.props.numRetweets}</span> 
				</div>
				<div 
				className={(this.state.pressFavorite) ? "ama" : ''}
				onClick ={this.onPressFavorite}
				>
				<span className="fa fa-star"> </span> 
				<span className="num">{this.props.numFavorites}</span> 
				</div>
			</div>
		</div>
		)	
	}
}

export default Message