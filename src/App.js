import React, { Component } from 'react';
import {
    Route,
    BrowserRouter
} from 'react-router-dom'
import 'normalize-css'
import './App.css';
import Header from './components/Header';
import Main from './components/Main';
import Profile from './components/Profile';
import Login from './components/Login';
import firebase from 'firebase'

class App extends Component
{
  constructor()
  {
    super()
    this.state= {
      user: null
    }

    this.handleOnAuth = this.handleOnAuth.bind(this)
    this.handleLogout = this.handleLogout.bind(this)
  }

  componentWillMount() {
    firebase.auth().onAuthStateChanged(user => {
      if(user){
        this.setState({ user: user })
      } else {
        this.setState({ user:null })
      }
    })
  }

  handleOnAuth()
  {
   const provider = new firebase.auth.GithubAuthProvider()
   firebase.auth().signInWithPopup(provider)
   .then(result => console.log(`${result.user.email} ha iniciado sesion`))
   .catch(error => console.log(`Error: ${error.code}: ${error.message}`))
  }

  handleLogout(){
    firebase.auth().signOut()
    .then(() => console.log("Te has desconectado correctamente") )
    .catch(() => console.log("Ha ocurrido un error") )
  }
render()
{
   return (
      <BrowserRouter>
        <div>
          <Header />

          <Route exact path='/'
           render= {() => {
                  if (this.state.user) 
                     {
                      return(
                        <Main
                            user={this.state.user}
                            onLogout = {this.handleLogout}
                         /> 
                        )
                        
                      } 
                else 
                      {
                         return(
                        <Login
                            onAuth={this.handleOnAuth}
                         /> 
                        )
                      }
            }}/>


        <Route  path='/profile'
           render={(props) => 
            <Profile
                picture={this.state.user.photoURL}
                username={this.state.user.email.split('@')[0]}
                displayName={this.state.user.displayName}
                location={this.state.user.location}
                emailAddress={this.state.user.email}
              /> }/>

          <Route path='/user/:username' 
           render={(props) => 
            <Profile
                picture={this.state.user.photoURL}
                username={this.state.user.email.split('@')[0]}
                displayName={this.state.user.displayName}
                location={this.state.user.location}
                emailAddress={this.state.user.email}
              /> }/>

        </div>
      </BrowserRouter>
    )
}

}


export default App;
