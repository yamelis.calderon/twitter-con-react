import React from 'react';
import ReactDOM from 'react-dom';
import firebase from 'firebase';
import './index.css';
import App from './App';

 // Initialize Firebase
  firebase.initializeApp({
    apiKey: 'AIzaSyAjc4GEA2BXkn2eUrcdlfKmg-zfhJwa3OY',
    authDomain: 'curso-react-8e008.firebaseapp.com',
    databaseURL: 'https://curso-react-8e008.firebaseio.com',
    projectId: 'curso-react-8e008',
    storageBucket: 'curso-react-8e008.appspot.com',
    messagingSenderId: '438231471674',
    appId: '1:438231471674:web:59158fb430d1002b060703',
    measurementId: 'G-0RJW4MMQCD'
  })

ReactDOM.render(<App />, document.getElementById('root'));


